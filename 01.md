# Capítulo 1. Pensar

> Es muy importante acostumbrarse a pedir aclaraciones al otro, a preguntarle detalles, a invitarle a definir sus tesis con precisión. Así, a la vez que se aprende a escuchar se ayuda a pensar al otro (Noel Clarasó)

## Introducción

Pensamos para ordenar nuestras experiencias. Pensamos para resolver problemas, teóricos y prácticos. Pensamos cómo ser mejores personas, cómo llevar una vida más sana, cómo alcanzar nuestras metas. Pensamos cuando leemos, cuando vemos el noticiero, cuando conversamos…¿Qué es lo que tienen en común estas actividades tan disímiles? La esencia del pensamiento, lo que todos los tipos de pensamiento tienen en común, es el orden. En el trasfondo de todo esfuerzo por pensar hay un esfuerzo por alcanzar el orden. Pensamos, básicamente, para poner orden en nuestras vidas. Una vida ordenada es una vida que se desarrolla en paz. La paz, decían los antiguos, es la tranquilidad en el orden; o la tranquilidad que da el orden. Traiga a su mente, por un momento, la paz que se respira en una casa ordenada, donde no hay nada fuera de lugar, donde hay un lugar para cada cosa y cada cosa está en su lugar. La vida prospera en el orden.

Compare, por un momento, esos dos escenarios de la vida futura de la humanidad que nos presentan las películas de ciencia ficción: por una parte, ciudades con edificios que se elevan hasta las nubes, con aeronaves que la atraviesan en forma ordenada. En estas ciudades se respira la paz. Por otra, ciudades desoladas y humeantes, contaminadas, oscuras, donde reina el caos y la ley del más fuerte. El pensamiento crítico aspira a llevar orden a esas ciudades que son nuestras vidas; a simplificar los problemas; a ayudarnos a ver con más claridad para distinguir lo valioso de lo que no merece la pena. No es, como algunos creen, afán malsano de llevar la contraria; es empeño por lograr mayor claridad, una mejor y más profunda comprensión de la realidad.

El hombre y la mujer formados en el hábito del pensamiento crítico son capaces de ver lo que otros no ven en los problemas del diario vivir, en las situaciones que se presentan ordinariamente en la empresa y en la vida de cualquier comunidad. Son capaces de situar los detalles relevantes en la imagen de conjunto. No pierden de vista la meta, la misión, pero son lo suficientemente flexibles para advertir cuando esta se está convirtiendo en una camisa de fuerza. Tienen lo que se llama una “mirada penetrante” de los problemas, y aportan soluciones que no se quedan en simple maquillaje, sino que van al fondo de las cosas.

Son humildes: nunca están seguros de sus propias intuiciones, y buscan el grano de verdad que toda afirmación o teoría contiene. Saben escuchar. Pratican continuamente el “principio de caridad”, que consiste en buscar la mejor interpretación de lo que los demás le dicen, no la peor. No pretenden ganar las discusiones. Son rápidos para admitir que su adversario tiene razón, cuando efectivamente la tiene. No les gusta la pelea; les gusta el acuerdo.

> Arrollar al adversario en materia filosófica, ¡que atrocidad! El verdadero filósofo sabe muy bien que no está instituido contra su adversario, sino que está instituido junto a su adversario y a los demás frente a una realidad siempre mayor y más misteriosa (Péguy)

> Yo no intento convencer a mi adversario de su error sino a alcanzar con él una más profunda verdad (Lacordaire)

Podemos decir, entonces, que pensar implica necesariamente criticar; que el pensamiento profundo y disciplinado es siempre crítico. Pero no “crítico” en el sentido de “hablar mal de alguien o de algo, o señalar un defecto o una tacha suyos”, sino en el de “analizar pormenorizadamente algo y valorarlo según los criterios propios de la materia de que se trate” (DLE).

Recordemos que el verbo “criticar”, en su origen, significa “separar, discernir, juzgar”. Está emparentado con el verbo cerno, que se encuentra, por ejemplo, en “discernir”. El pensamiento crítico, por lo tanto, es aquel que sabe separar lo bueno de lo malo, lo valioso de lo inútil, lo verdadero de lo falso. Es una habilidad que todos debemos cultivar.

> La abstracción es un proceso mental que usamos al tratar de discernir lo que es esencial o relevante de un problema (Tom G. Palmer)

## Pensamiento crítico y libertad

El pensamiento crítico no solo nos ayuda a poner orden en nuestras vidas; también nos ayuda a ser libres. En efecto, ser libres implica poder hacer lo que queremos; pero para eso necesitamos disponer de información confiable, y la mejor manera de obtener esa información es aplicando las técnicas del pensamiento crítico.

Suponga que usted va al supermercado a comprar leche. Observa que la leche semidescremada contiene “solo dos por ciento de grasa”, y piensa que probablemente le conviene comprar esa leche porque quiere adelgazar. Claro, la leche semidescremada es un poco más cara, pero usted piensa que es un gasto que merece la pena hacer. Meses más tarde, descubre que la leche entera tiene un tres por ciento de grasa, y se siente engañado.

En el ejemplo anterior, vemos que muchas veces, sin darnos cuenta, creemos ser libres, pero, de hecho, no lo somos. Siendo un poco más críticos, habríamos razonado más o menos así: “dos por ciento de grasa…, ¿en comparación con qué? ¿Es un dos por ciento en relación con la leche entera? ¿Quiere eso decir que, si la leche entera tiene un cien por ciento de grasa, la semidescremada tiene solo un dos por ciento? ¿O deberíamos leer ese porcentaje como “la leche semidescremada contiene dos por ciento de grasa, doce por ciento de X, sesenta por ciento de agua, etc.?”

Otro ejemplo:

Usted tiene una plaga de ratas en su casa y piensa: voy a contratar a la empresa X. Seguramente que me libraré de esa plaga.

Sin darse cuenta, su razonamiento presupone una premisa que no explicita: que la empresa X es buena exterminando roedores. Tal vez se dejó llevar usted por la publicidad, y no pidió pruebas de su trabajo a la empresa X.

Muchas veces tomamos decisiones equivocadas por precipitación; por no examinar los problemas desde todos sus ángulos, o por confiar demasiado en nuestra intuición.

La precipitación es hija de la pereza. La pereza se combate con diligencia. “Diligencia” procede del latín diligere que significa ‘amar’, poner cuidado y cariño en lo que se hace. Y si no queremos ser esclavos de nuestros errores, si no queremos padecer las consecuencias de nuestras decisiones precipitadas, debemos combatir la pereza; la pereza mental. El hábito del pensamiento crítico nos libera de nuestras malas decisiones, al menos, la mayoría de veces.

## El poder de los prejuicios

Otro enemigo imponente de nuestra libertad son los prejuicios. Todos tenemos prejuicios, y resulta prácticamente imposible “eliminar todo lo que huela a humano de nuestras teorías e interpretaciones” (J. Conant). Pero del hecho de que no existan interpretaciones puras o no contaminadas, de que no podamos ver las cosas desde “el punto de vista del ojo de Dios” (H. Putnam), no significa que debamos renunciar al esfuerzo por ser lo más objetivos que podamos. Hablamos, desde luego, de una objetividad hasta donde nos es posible alcanzarla.

Un ejemplo claro de cómo los prejuicios obnubilan nuestro juicio se presenta en la película Twelve Angry Men *(Doce hombres en pugna*), del director Sidney Lumet (1957). En ella, los doce miembros de un jurado deben dictaminar sobre la inocencia o culpabilidad de un joven latino acusado de asesinar a su padre. A lo largo de la película se aprecia que cada miembro del jurado tenía un particular prejuicio que le hacía creer más en las “pruebas” y testimonios que inculpaban al joven que en las que lo exoneraban de culpa. Solo un miembro del jurado hace el esfuerzo por pensar objetivamente.

12 Angry Men (1957)

> Muchas personas creen que están pensando cuando solamente están reorganizando sus prejuicios (William James*)

De manera que el primer objeto al que debemos aplicar las técnicas del pensamiento crítico es nuestro propio sistema de creencias.

## Creencias y pensamiento crítico

Sin un conjunto de creencias básicas sobre el hombre, el mundo, la sociedad, la política y la economía no podríamos vivir. Ciertamente, debemos ser un tanto escépticos* sobre nuestras creencias, pero no hasta el punto de que no podamos tomar decisiones. El filósofo austríaco Sir Karl Popper* decía que, si bien no podemos afirmar con seguridad que una teoría sea verdadera, al menos podemos decir que es no falsa, y podemos seguir apoyándonos en ella hasta que se demuestre que es falsa. Es, ni más ni menos, que la aplicación del princpio clásico de presunción de inocencia a nuestras teorías: todos somos inocentes hasta que se demuestre lo contrario. Pero el pensamiento crítico nos motiva a someter a prueba nuestras creencias, y a estar pendientes de todo indicio de “culpabilidad”.

Podemos hacer la analogía con el funcionamiento de una máquina. Nuesro sistema de creencias nos guía por la vida de manera más o menos segura. Pero debemos estar atentos a las señales de alarma, a las luces rojas que se prenden en el tablero de control. Esas luces rojas son, en nuestras vidas, las contradicciones y los contraejemplos.

Supongamos que usted ha creído toda su vida que los países ricos deberían donar comida a los países pobres. Un día, ve un documental que muestra cómo esa asistencia perjudica a los productores locales y retrasa el desarrollo de los países pobres. Entonces, piensa que posiblemente debe revisar sus creencias, al menos en materia económica.

Mientras que con nuestras creencias y razonamientos debemos ser críticos, con los de los demás debemos ser caritativos. Esto significa que en los argumentos de los demás debemos siempre buscar la mejor interpretación posible, la que lo deje en una mejor posición, y no la peor o la más débil. Solo así es posible el diálogo constructivo. 

La falacia del “muñeco de paja” consiste en buscar los puntos más débiles de la argumentación del contrincante y construir con ellos un argumento vulnerable. Destruimos, entonces, esa posición, y quines nos escuchan terminan creyendo que vencimos a nuestro oponente.

El pensamiento crítico es una herramienta, un hábito mental, cuyo fin es a verdad y la objetividad, en la medida en que estas sean posibles. Buscamos la verdad porque solo con ella somos libres. En medio de la desinformación, la mentira, el engaño y los prejuicios no prospera la libertad, ni individual ni política.

## Pensamiento y escritura

¿Cómo aprender a pensar bien, con orden, claridad, coherencia, fuerza y hasta elegancia? Un medio seguro para alcanzar esa meta es ejercitarnos en la escritura.

El nexo entre pensamiento y lenguaje es evidente. Podemos decir que el lenguaje es el vehículo del pensamiento; sin ese vehículo, el pensamiento no avanza, se estanca. Pues bien: si escribimos nuestro pensamiento podemos “ver”, por decirlo así, hacia dónde nos conducen nuestras ideas. En la expresión oral, las ideas se desvanecen tan pronto se pronuncian; la exitgencia del orden es menor. Se producen muchas digresiones y hasta se retrocede. En cambio, el lenguaje escrito es más exigente. Todo lo que escribimos debe tener una secuencia lógica, que conduzca a la meta que al principio anunciamos.

Escribir, por lo tanto, es un excelente ejercico para nuestra mente. Al escribir, nos forzamos a pensar.

Si escribir es pensar, escribir bien es pensar bien. (Lo contrario, lamentablemente, no es necesariamente cierto). Por eso, en muchas universidades, la argumentación se enseña en los cursos de escritura.

> Escribir es una forma concentrada de pensar (Donald DeLillo)

## Pensamiento crítico y educación

La buena educación no consiste en enseñar qué pensar, sino en enseñar a pensar. Enseñar qué pensar es adoctrinamiento. Enseñar a pensar por sí mismo es más difícil que enseñar “contenidos”, datos, información. En nuestro tiempo —en realidad, en cualquier tiempo—, no necesitamos que nos llenen la cabeza de datos y de información, sino que nos enseñen a discriminar la información. La meta del proceso educativo, recordémoslo, es formar hombres y mujeres libres, que han hecho suyas las creencias y valores que sostienen, que saben dar razón de sus convicciones.

> ¿Dónde está la sabiduría que se nos ha perdido en conocimiento? ¿Dónde está el conocimiento que se nos ha perdido en información? (T. S. Elliot)

Una revista de educación chilena da una buena explicación de por qué el pensamiento crítico se consiera una habilidad indispensable en nuestro siglo:
La era de la información y el conocimiento nos exige un doble desafío. En primer lugar, la mayoría de los trabajos —y cada vez más— implican rendir cuentas, entender y procesar información. En segundo lugar, la mayoría de las actividades relacionadas con las comunicaciones y la información se han trasladado a Internet, en donde la confiabilidad de la información se vuelve incierta.

Para hacer frente a estos desafíos, se hace necesario enseñar el pensamiento crítico, lo que implica realizar juicios documentados, discriminar la calidad de la información, exponer correcta y ordenadamente y pensar de manera autónoma. El pensamiento crítico es un ejercicio del intelecto que permite realizar juicios y tomar decisiones luego del análisis, evaluación y contraste de argumentos, afirmaciones, puntos de vista y evidencias.

En el quehacer educativo, esto implica consideraciones como presentar la mayor cantidad posible de puntos de vista alternativos, encargar la recolección de fuentes de información diversas para su interpretación, la búsqueda de conexiones entre las piezas de material reunidas y la elaboración de conclusiones a partir de los mejores análisis. La síntesis previa de cada fuente es un ejercicio que puede ayudar a asimilar mejor la información y los puntos de vista. Podemos llevar algunos de estos pasos al plano del debate, de modo que la resolución sea colaborativa.

El pensamiento crítico no es lo mismo que un debate desinformado, la opinión, la preferencia o el juicio exante. Junto con la creatividad, el pensamiento crítico es una habilidad que nos ayuda a tomar decisiones y resolver los problemas complejos conocidos y aquéllos con los que todavía no nos familiarizamos y que deberán resolver el en futuro nuestros alumnos.

Algunos lineamientos que se pueden tomar en consideración al momento de reenfocar las clases hacia el desarrollo del pensamiento crítico:

-Exponer menos y dar más espacio al pensamiento de los estudiantes.

-Enseñar a leer, analizar y procesar la información en lugar de hacerlo por ellos.

-Presentar temas complejos con sus conceptos y posibles problemas a resolver.

-Razonar en voz alta frente de los alumnos del modo en que queremos que ellos lo hagan.

-Preguntar frecuentemente por las dimensiones de su pensamiento: su propósito, evidencia, razones, datos, afirmaciones, creencias, interpretaciones, deducciones, conclusiones, implicancias y consecuencias de su pensamiento.

-Fortalecer la atención en los puntos de vista de los demás estudiantes ofreciendo la palabra a quienes no levantan la mano y pidiendo a los demás sintetizar lo que dijo.

-Usar ejemplos concretos con situaciones que resulten familiares para los estudiantes para ilustrar problemas y conceptos complejos.

-Favorecer las actividades en grupos pequeños con tareas y tiempos específicos y pedir rendición de cuentas sobre qué hicieron, qué problemas surgieron y cómo los resolvieron.

-Solicitar regularmente la escritura de trabajos y ensayos, entre los cuales pueda sortearse una muestra a ser revisada por el profesor.

-Explicar los propósitos, el método y las formas de evaluación de la asignatura antes de comenzar las clases.

Fuente de la tabla: www.criticalthinking.org

(«Educarchile - Habilidades del Siglo XXI - Pensamiento Critico», s. f.)

## Plan de este libro

El primer paso para pensar con claridad y ser verdaderamente críticos, es establecer el problema y la tesis de un argumento. Sin tesis no hay argumento. Veremos cómo identificar la tesis y los tipos de problemas que podemos abordar.

Una tesis se sostiene con razones. En el capítulo tercero estuiaremos los diferentes tipos de razones, que vienen a ser las premisas de un argumento,

Antes de seguir analizando la estructura básica de un argumento, hablaremos, en el cuarto capítulo, de la claridad y de su enemigo: la ambigüedad. Antes, incluso, de evaluar las razones que se dan para para sostener una tesis o conclusión, es necesario evitar las malinterpretaciones, que se dan, precisamente, por no abordar las posibles ambigüedades.

El capítulo quinto aborda un tema de gran importancia: los presupuestos de los que partimos al argumentar. Por definición, los presupuestos (o supuestos) están implícitos en la argumentación. Alguien que se ha formado en las técnicas del pensamiento crítico sabe descubrir los presupuestos, y hace ver las consecuencias que necesariamente se derivan de ellos. Una persona honrada intelectualmente, cuando ve que, a lo mejor, parte de supuestos equivocados, se apresura a rectificar.

El siguiente capítulo (sexto) se dedica a un tema que no puede faltar en todo libro de razonamiento crítico (o, como se llamaba antes, lógica informal): las falacias. Aunque veremos algunas falacias por su nombre, más importante que identificarla es saber decir por qué un razonamiento dado es falaz, es decir, falso o defectuoso. Veremo en este capítulo que es lo que todas las falacias tienen en común, y cómo desmontarlas.

El capítulo séptimo tiene por tema la evaluación de la evidencia o de las fuentes de información. No es lo mismo, evidentemente, la evidencia que debemos presentar para un argumento científico o descriptivo, que para uno filosófico. En el primer caso, recurrimos a datos, tomados de fuentes recientes y confiables; en el segundo, buscaremos los textos más relevantes, de ediciones confiables.

Siguiendo con la evaluación de la evidencia, el siguiente capítulo (octavo) se dedica a mostrarnos las formas en las que podemos ser víctimas de engaños con datos o estadísticas alteradas. O, más bien, con la forma en que se presentan los datos.

También podemos ser víctimas de la omisión de información. Ese es el tema del noveno capítulo. En la publicidad, en las noticias y en cualquier forma de información, tan importante o más importante aun que los datos que nos presentan son los que no nos presentan.

El título del décimo capítulo deja ver cuál es su contenido: “Otras posibles causas”. Sucede que un fenómeno raramente se explica por una sola causa, principalmente si es social (no físico). Los científicos saben muy bien que concurrencia no significa causalidad, y se cuidan mucho de afirmar que “la causa de tal fenómeno es X”. El pensador crítico tiene como norma no apresurarse en sus conclusiones, tratar de ver más allá de lo obvio, para no caer en la trampa de comter la falacia, tan frecuente, del “post hoc (después de eso) ergo (por lo tanto) propter hoc (debido a eso)”.

El undécimo capítulo se titula “Otras posibles conclusiones” y, como es de esperarse, intenta mostrar la importancia de preguntarnos si, con las mismas premisas y datos, no puede llegarse a conclusiones distintas a las más obvias o deseadas.

Como decía Bertrand Russell, en todo escrito hay que decir qué se va a decir, decirlo y decir que se ha dicho. El último capítulo de este libro es un repaso de los temas vistos a lo largo del libro. Una síntesis muy rápida, para quien ya ha profundizado en los temas y quiere quedarse con una especie de prontuario de pensamiento crítico.

El apéndice contiene las respuestas a los ejercicios seleccionados, como suele ser costumbre en los manuales universitarios.

REFERENCIAS
Educarchile - Habilidades del Siglo XXI - Pensamiento Critico. (s. f.). Recuperado 11 de marzo de 2018, a partir de http://www.educarchile.cl/ech/pro/app/detalle?id=219623
